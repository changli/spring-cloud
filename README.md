# spring-cloud

#### 项目介绍
SpringCloud整合zuul路由,eureka注册中心,hystrix熔断器简单使用示例，[参考资料](https://github.com/forezp/SpringCloudLearning)

#### 软件架构
SpringCloud微服务系统，配置erueka注册发现，hystrix熔断器处理，zuul路由配置，本处只是简单实例，eureka没有配置集群方式，使用单机版。


#### 安装教程

1. git下载源码，导入idea或者eclipse
2. 编译源码项目，依次启动eureka------>consumer---->producer-->zuul


#### 使用说明

1. 启动成功，浏览器访问:http://localhost:8889/producer/hello?name=apple
2. 刷新浏览器，会出现两种请求响应结果，表明zuul已经生效。
3. 浏览器访问:http://localhost:9001/hello/apple
4. 关闭producer和producer2两个项目。
5. 再次请求：http://localhost:9001/hello/apple
6. 步骤5页面显示发送失败，表示熔断器已经生效。
7. 启动成功，浏览器访问：http://localhost:9001/upload/apple
8. 浏览器输出上传文件的信息，表示springCloud通过feign上传文件成功

#### 友情链接

1. 邮箱地址: m15171479289@163.com 
2. github：https://github.com/apple987