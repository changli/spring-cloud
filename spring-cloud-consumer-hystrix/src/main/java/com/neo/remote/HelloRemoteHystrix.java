package com.neo.remote;

import com.neo.remote.HelloRemote;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

/**
 * 简单熔断器
 */
@Component
public class HelloRemoteHystrix implements HelloRemote{

    @Override
    public String hello(@RequestParam(value = "name") String name) {
        return "你好 " +name+", 消息发送失败 ";
    }

	@Override
	public String upload(@RequestPart(value = "file") MultipartFile file) {
		return "您好，服务器繁忙，上传文件失败，请稍候重试！";
	}
}
