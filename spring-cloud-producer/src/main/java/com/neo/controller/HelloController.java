package com.neo.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;

@RestController
public class HelloController {
	
    @RequestMapping(value="/hello")
    public String index(@RequestParam String name) {
        return "你好 "+name+"，这里是spring-cloud-producer响应结果，消息发送成功";
    }
    
    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String upload(@RequestPart(value = "file") MultipartFile file) throws IllegalStateException, IOException {
    	List<String> arr=org.apache.commons.io.IOUtils.readLines(file.getInputStream(),"utf-8");
        System.err.println("读取文件内容是:"+JSON.toJSONString(arr));
        String resp="上传文件名称:"+file.getName()+"\t 文件内容:"+JSON.toJSONString(arr);
    	return resp;
    }
}